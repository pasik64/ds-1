# DS-Leader election (DS-1, DS-2)

## Volba leadera a obarvení uzlů

Na začátku svého běhu si každý uzel vygeneruje své vlastní uuid. Pomocí UDP broadcastu pošle všem uzlům zprávu se svým uuid a 
ostatní uzly mu odpoví posláním zprávy, která obsahuje také jejich uuid. Díky tomu má každý uzel k dispozici seznam všech uuid.
Za leadera je považován uzel, jehož uuid hash je nejvyšší. Barvu si každý uzel zjistí podle toho, kolikátý nejvyšší uuid hash má. 
První třetina uzlů je zelená, zbývající uzly jsou červené.

## Spuštění aplikace a ověření funkčnosti

Aplikaci je možné spustit pomocí příkazu vagrant up v kořenovém adresáři projektu. Jednotlivé uzly poté do logů vypíší, jakou mají barvu a pokud je daný uzel leader, vypíše i informaci o tom, že je leader. Počet uzlů je možné změnit ve vagrantfilu změnou konstanty NODES_COUNT.

## Komunikační náročnost

Vzhledem k tomu, že veškerá komunikace mezi uzly je řešena pomocí UDP broadcastu, dochází k odesílání velkého množství zpráv, které nemusejí být vždy potřebné. Velký prostor pro zlepšení by mohl být ve využití master uzlu, jelikož jeho role je v práci nulová. Je pouze zvolen, nicméně sám o sobě nedělá nic jiného, než co dělají ostatní uzly. S jeho lepším využitím by bylo možné částečně optimalizovat množství zpráv tak, aby jich nemuselo být odesíláno tak velké množství.
