VAGRANTFILE_API_VERSION = "2"
# set docker as the default provider
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'docker'
ENV['FORWARD_DOCKER_PORTS'] = "1"
# minor hack enabling to run the image and configuration trigger just once
ENV['VAGRANT_EXPERIMENTAL']="typed_triggers"

unless Vagrant.has_plugin?("vagrant-docker-compose")
  system("vagrant plugin install vagrant-docker-compose")
  puts "Dependencies installed, please try the command again."
  exit
end

# Names of Docker images built:
NODE_IMAGE  = "ds/ds-1/node:0.1"


NODES  = { :nameprefix => "ds-1-", 
              :subnet => "10.0.1.",
              :ip_offset => 100, 
              :image => NODE_IMAGE,
              :port => 5000 }
# Number of backends to start:
NODES_COUNT = 5

# Common configuration
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Before the 'vagrant up' command is started, build docker images:
  config.trigger.before :up, type: :command do |trigger|
    trigger.name = "Build docker images and configuration files"
    trigger.ruby do |env, machine|

      # Build image for backend nodes:
      puts "Building backend node image:"
      `docker build backend -t "#{NODE_IMAGE}"`
    end
  end

  # Definition of N backends
  (1..NODES_COUNT).each do |i|
    node_ip_addr = "#{NODES[:subnet]}#{NODES[:ip_offset] + i}"
    node_name = "#{NODES[:nameprefix]}#{i}"
    # Definition of BACKEND
    config.vm.define node_name do |s|
      s.vm.network "private_network", ip: node_ip_addr
      s.vm.hostname = node_name
      s.vm.provider "docker" do |d|
        d.image = NODES[:image]
        d.name = node_name
        d.has_ssh = true
      end
      s.vm.post_up_message = "Node #{node_name} up and running on #{node_ip_addr}. You can access the node with 'vagrant ssh #{node_name}'}"
    end
  end

end

# EOF
