from flask import Flask

import socket
import uuid

app = Flask(__name__)
myID = str(uuid.uuid4())
print("init - uuid: " + myID, flush=True)

sock = socket.socket(socket.AF_INET,
            socket.SOCK_DGRAM,
            socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) 
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
sock.bind(("", 31111))
MESSAGE = "Node-registration:" + myID
sock.sendto(MESSAGE.encode('utf-8'), ('<broadcast>', 31111))
sock.settimeout(20)
NodeList = []
try:
    while True:
        data, addr = sock.recvfrom(1024)
        if (data.startswith(b"Node-registration:")):
            NodeList.append((str(data.split(b':')[1])).split('\'')[1])
            WelcomeMessage = "Hello from:" + myID
            sock.sendto(WelcomeMessage.encode('utf-8'), ('<broadcast>', 31111))
        elif (data.startswith(b"Hello from:")):
            if (str(data.split(b':')[1])).split('\'')[1] not in NodeList:
                NodeList.append((str(data.split(b':')[1])).split('\'')[1])
        elif (data.startswith(b"Alive:")):  
            break      
except socket.timeout:
    pass

betterNodes = 0
greenCount = len(NodeList) / 3

if (len(NodeList) % 3 != 0):
    greenCount += 1 
for node in NodeList:
    if (node.__hash__ > myID.__hash__):
        betterNodes += 1


if (betterNodes == 0):
    print("state: leader", flush=True)
    print("color: green", flush=True)

else:
    if (betterNodes < int(greenCount)):
        print("color: green", flush=True)
    else:
        print("color: red", flush=True)    

sock.settimeout(5)

ProblematicNodes = {}
while True:
    AliveMessage = "Alive:" + myID;
    sock.sendto(AliveMessage.encode('utf-8'), ('<broadcast>', 31111))
    AllNodes = NodeList.copy()
    try:
        while True:
            data, addr = sock.recvfrom(1024)
            print(data, flush=True)
            if (data.startswith(b"Alive:")):
                if (str(data.split(b':')[1])).split('\'')[1] in AllNodes:
                    AllNodes.remove((str(data.split(b':')[1])).split('\'')[1])
                if (str(data.split(b':')[1])).split('\'')[1] in ProblematicNodes:
                    del ProblematicNodes[(str(data.split(b':')[1])).split('\'')[1]]
            if (data.startswith(b"HEALTH")):    
                sock.sendto(("OK\n").encode("utf-8"), addr);        
    except socket.timeout:
        pass

    MissingNodes = []

    for node in AllNodes:
        if node in ProblematicNodes:
            ProblematicNodes[node] += 1
            if (ProblematicNodes[node] == 5):
                MissingNodes.append(node)
        else:
            ProblematicNodes[node] = 1

    if (len(MissingNodes) != 0):
        for node in MissingNodes:
            NodeList.remove(node)
        print("Some nodes missing. Recalculating...")  
        betterNodes = 0
        greenCount = len(NodeList) / 3

        if (len(NodeList) % 3 != 0):
            greenCount += 1 
        for node in NodeList:
            if (node.__hash__ > myID.__hash__):
                betterNodes += 1
        if (betterNodes == 0):
            print("state: leader", flush=True)
            print("color: green", flush=True)

        else:
            if (betterNodes < int(greenCount)):
                print("color: green", flush=True)
            else:
                print("color: red", flush=True)           
